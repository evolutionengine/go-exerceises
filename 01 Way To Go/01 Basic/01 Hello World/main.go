//Declaring the 'main' package
package main

//importing package as required
//fmt stands for "format", this is a predefined package in golang
import "fmt"

//declaring the main function
//function main is the starting point of execution of the program
func main() {
	//printing message on terminal
	fmt.Println("Hello, World!")
}

//Run the program from the current directory in terminal by typing $ - go run main.go
