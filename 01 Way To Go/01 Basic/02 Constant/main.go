package main

import "fmt"

//NAME - Declaring a constant, this is visible to all functions in the file
//The value of constant cannot be altered
//Generally const are all CAPITAL
const NAME = "Anil"

//Alternate method o declaring const
//First use of iota gives a value of "0", and increments by 1
//iota is a predefined identifier in golang and used for ease of incrementing value
const (
	a = iota
	b
	c
)

//Init is initialized first before executing main
func init() {

}

func main() {
	fmt.Println("Hello", NAME)
	fmt.Println(a, b, c)
}
