package main

//You can import multiple packages using the following syntax
// os - predefined package in golang for interacting with operating system
import (
	"fmt"
	"os"
)

func main() {

	//Declaring variables
	var a = 1
	var b = true
	var c = "Anil"

	//printing variables
	fmt.Printf("The value of a is: %v\nThe value of b is : %t\nThe value of c is :%v\n\n", a, b, c)

	//note we are using Printf instead of Println, its just a different syntax
	// "\n" is for escaping to new line

	//Alternate declaring method
	var d int = 2
	var e bool = false
	var f string = "Kulkarni"

	fmt.Println("The value of d is:", d, "\nThe value of e is :", e, "\nThe value of f is :", f)
	//after printing "c" it automatically goes to new line
	fmt.Printf("\n")

	//Preferred method(type inference), the compiler automatically sets the type
	g := 3
	h := true
	i := "Learning Golang"

	fmt.Printf("The value of g is: %v\nThe value of h is : %t\nThe value of i is :%v", g, h, i)
	fmt.Println()
	//Multiple variables can also be declared as follows
	//Note we are also using predefined functions from "os" package to get information
	var (
		HOME   = os.Getenv("HOME")
		USER   = os.Getenv("USER")
		GOROOT = os.Getenv("GOROOT")
		GOOS   = os.Getenv("GOOS")
	)

	fmt.Printf("The operating system is %v and the user is %s \n", GOOS, USER)
	fmt.Printf("The home directory is %s and the go root is at %v \n", HOME, GOROOT)
}
