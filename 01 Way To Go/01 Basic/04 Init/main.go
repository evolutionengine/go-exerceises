package main

import (
	"fmt"
	//importing the "trans" package from the folder "trans" which is in the same parent directory
	"./trans"
)

//creating a variable to compute our desired value
var twoPi = 2 * trans.Pi

func main() {
	//before it runs main(), it will run init() in trans and compute value of Pi
	fmt.Printf("2 x Pi = %g\n", twoPi)
}
