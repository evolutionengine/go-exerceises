//Package trans - Creating a custom package "trans" for testing init
//note that the file init.go is places in a seperate folder named "trans"
//also here the name of package is "trans", this is how golang identifies and imports packages
package trans

//importing the "math" from Std Library for calculating Pi
import "math"

//Pi - declaring variable for computation
var Pi float64

func init() {
	//there can be only one init() in a file
	//calucating the value of Pi
	//init() is always executed first
	Pi = 4 * math.Atan(1)
}
