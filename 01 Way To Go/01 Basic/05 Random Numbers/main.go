package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	//generating 10 random numbers directly using the built-in function in rand package
	for i := 0; i < 10; i++ {
		a := rand.Int()
		fmt.Println("The randon number is:", a)
	}
	fmt.Println()

	//generating randon number by giving an integer input
	for i := 0; i < 10; i++ {
		a := rand.Intn(8)
		fmt.Println("The randon number is:", a)
	}
	fmt.Println()

	//We now set the reference for generating randon numbers
	//The reference is taken as the system time in nano seconds
	timens := int64(time.Now().Nanosecond())
	//Seed is a built-in function in rand package
	rand.Seed(timens)

	//Now generating random numbers and displaying with 2 point accuracy in floats
	for i := 0; i < 10; i++ {
		fmt.Printf("The randon number is: %2.2f\n", 100*rand.Float64())
	}
	fmt.Println()
}
