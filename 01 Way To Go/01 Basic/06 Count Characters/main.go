//We will be counting the characters (runes) and bytes
package main

import (
	"fmt"
	"strings"
	//importing "strings" & "unicode/utf8" packages as we would utilize their built-in functions
	//"Strings & unicode/utf8" are part of std lib provided by golang

	"unicode/utf8"
)

func main() {
	//input will be in string format
	str := "asSASA ddd dsjkdsjs dk"

	for len(str) > 0 {
		//wondering how did I know the following function like DecodeRuneInString() ?
		//well I didn't know, I just read the documentation at golang.org/pkg/utf8
		//reading package documentation is the only way to know built-in functions, always refer them.
		r, size := utf8.DecodeRuneInString(str)
		//r & size are return value by the function DecodeRuneInString, we capture them to print data
		fmt.Printf("%c %v\n", r, size)

		str = str[size:]
	}

	fmt.Println("New String Output:")

	//changing the string
	str = "asSASA ddd dsjkdsjsこん dk"

	for len(str) > 0 {
		r, size := utf8.DecodeRuneInString(str)
		fmt.Printf("%c %v\n", r, size)

		str = str[size:]
	}

	fmt.Println()
	//counting characters
	str = "Hello, World!"
	fmt.Printf("Number of characters in '%s' are: ", str)
	fmt.Printf("%d\n", strings.Count(str, ""))
}
