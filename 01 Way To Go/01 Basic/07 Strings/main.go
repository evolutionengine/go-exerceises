package main

import (
	"fmt"
	"strings"
)

func main() {
	//We will see some of the different operations which can be carried out on strings
	//Read entire documentation at golang.org/pkg/strings

	//Declaring a string
	str := "My dog's name is Bruno"
	//Check for a sub string in a string
	fmt.Println(strings.Contains(str, "Bruno"))
	fmt.Println()
	//Check for a particular character in string
	fmt.Println(strings.ContainsAny(str, "i"))
	fmt.Println()
	//Check is strings are equal under unicode case folding
	//Returns a boolean value
	fmt.Println(strings.EqualFold("Equal", "equal"))
	fmt.Println()
	//Joining strings
	strNew := []string{"My", "name", "is", "Anil", "Kulkarni"}
	fmt.Println(strings.Join(strNew, " "))
	fmt.Println()
	//Converting string into slice
	strGo := "Golang"
	a := []rune(strGo)
	//Printing from slice
	for i, v := range a {
		fmt.Printf("Character at position no. %v is: %c\n", i+1, v)
	}
	fmt.Println()
	//It also works on unicode
	s := "Hello, 世界"
	for i, v := range s {
		fmt.Printf("Character at position no. %v is: %c\n", i+1, v)
	}
	fmt.Println()
}
