package main

import (
	"fmt"
	"strconv"
)

func main() {
	//Convert from string to int
	//Note that 10 has been declared as string
	v := "10"
	if s, err := strconv.Atoi(v); err == nil {
		fmt.Printf("Type: %T\tValue: %v\n", s, s)
	}

	//Convert from int to string
	i := 20
	s := strconv.Itoa(i)
	fmt.Printf("Type: %T\tValue: %v\n", s, s)

}
