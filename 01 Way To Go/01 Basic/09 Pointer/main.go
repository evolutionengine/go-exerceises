//Pointers are special data type used to store the address of data
package main

import (
	"fmt"
)

func main() {
	var i1 = 5
	//print the address where the value "5" of the integar variable i1 is stored
	fmt.Printf("Value of the var i1 is: %v and it is stored in the memory at: %p\n", i1, &i1)
	fmt.Println()

	//This address can be stored in a pointer
	var pointerAddress *int
	pointerAddress = &i1

	//Pointers are cheap and greatly help to get better performance and speed of execution
	//Pointers in 64 bit m/c only takes 8 bytes
	fmt.Printf("Value of the var pointerAddress is: %v and it is stored in the memory at: %v\n", *pointerAddress, pointerAddress)

	//Both operations give the same memory address
	fmt.Println()

	//Changing the value in pointer also changes the value in the original variable
	s := "First value"
	fmt.Printf("The original value of s is: %v\n", s)
	p := &s
	*p = "Second value"
	fmt.Printf("Now the value of s is: %v\n", s)

	fmt.Println()
	//Pointers cannot modify the value of const
	//Following will not work
	/*
	  const s1 = "Some Value"
	  p2 := &s1
	  *p2 = "New Value"
	*/

	//Arithmetic operations are now allowed on pointers in Golang
	//b := *pointerAddress + 2 -> Invalid Code
}
