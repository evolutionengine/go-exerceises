package main

import "fmt"

func main() {
	bool1 := true

	if bool1 {
		fmt.Printf("This is true\n")
	} else {
		fmt.Printf("This is false\n")
	}
	//Initializing var in 'if' and checking for condition
	if cond := 5; cond > 10 {
		fmt.Println("Cond is true")
	} else {
		fmt.Println("Cond is false")
	}

	fmt.Printf("Auto complete works in vs code!\n")
}
