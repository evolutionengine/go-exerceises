package _1_Simple

import (
	"fmt"
	"strconv"
)

func main() {
	//Error reporting is very important
	//Functions in golang normally return two Values - 1) The Value & (2) True or False
	orig := "ABC"
	an, err := strconv.Atoi(orig)
	if err != nil {
		fmt.Printf("Orig %s is not an integer - Exiting with error\n", orig)
		return
	}
	fmt.Printf("The integer is %d", an)

	//The error value can be dicarded using an '_'
	ab, _ := strconv.Atoi(orig)
	//'_' can be used anywhere when value has to be dicarded
	fmt.Printf("%v", ab)
	//Above statement will not print anything as ab is not an integer
}
