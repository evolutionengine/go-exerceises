package main

import (
	"fmt"
	"errors"
)
//Declaring a new error type using the package Errors
var errorNotFound = errors.New("Error not found")

func main() {
	fmt.Printf("Error: %v\n", errorNotFound)
}

