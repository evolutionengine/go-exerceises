package main

import "fmt"

func main() {
	fmt.Println("Starting the program.")
	panic("A severe error occured, exiting the program!")
	fmt.Println("Shutting the program!")
}
