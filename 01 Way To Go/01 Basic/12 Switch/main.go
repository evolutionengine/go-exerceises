package main

import "fmt"

//Calculate() - New function to calculate new num and return the value
func calculate(a int64) int64 {
	return a * 2
}

func main() {
	//Declaring a variable to store our number
	var num int64
	//Asking for user input
	fmt.Printf("Enter any no: ")
	//Scanning and saving the user input in variable
	fmt.Scan(&num)
	//Swtiching according to the inout value
	switch {
	case num < 100:
		fmt.Println("No. is less than 100")
	case num == 100:
		fmt.Println("No. is equal to 100")
	default:
		fmt.Println("No. is neither less than 100 or equal to 100")
	}

	//using functions in switch
	var newNum int64
	fmt.Printf("Enter a new no: ")
	fmt.Scan(&newNum)
	//Declaring a variable and assigning it to value returned by function
	switch result := calculate(newNum); {
	case result > 0 && result < 20:
		fmt.Printf("The new no. %d is between 0 & 20\n", result)
	case result > 20 && result < 100:
		fmt.Println("The new no.", result, "is between 20 & 100")
	case result == 100:
		fmt.Println("The new no. is equal to 100")
	default:
		fmt.Println("The new no. is more than 100")
	}
}
