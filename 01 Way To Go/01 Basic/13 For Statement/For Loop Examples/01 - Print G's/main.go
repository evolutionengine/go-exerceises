/*Aim - To print
G
GG
GGG
...till 25 characters */
package main

import "fmt"

func main() {
	char := "G"
	str := char
	for i := 0; i < 24; i++ {
		fmt.Printf("%s\n", str)
		str = str + "G"
	}
	//Just checking if the condition is fulfilled
	fmt.Printf("The length of str is: %v\n", len(str))
}
