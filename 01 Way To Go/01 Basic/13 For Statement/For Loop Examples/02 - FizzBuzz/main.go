//Print Fizz Buzz when no is multiple of 3 & 5
//Print Fizz when no is multiple of 3
//Print Buzz when no is multiple of 5
package main

import "fmt"

func main() {
	num := 1

	for i := 0; i < 100; i++ {
		switch {
		case num%5 == 0 && num%3 == 0:
			fmt.Println("FizzBuzz")
		case num%3 == 0:
			fmt.Println("Fizz")
		case num%5 == 0:
			fmt.Println("Buzz")
		default:
			fmt.Println(num)
		}
		num++
	}

}
