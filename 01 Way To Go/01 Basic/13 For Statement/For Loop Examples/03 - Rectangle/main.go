//Print a rectangle of *
//Width = 20, height = 10
package main

import "fmt"

func main() {
	var (
		width  int
		height int
	)

	fmt.Printf("What is the width of rectangle: ")
	fmt.Scan(&width)

	char := "*"
	str := ""
	fmt.Printf("What is the height of rectangle: ")
	fmt.Scan(&height)

	fmt.Println()

	for i := 0; i < width; i++ {
		str = str + char
	}

	for j := 0; j < height; j++ {
		fmt.Println(str)
	}

	fmt.Println()
}
