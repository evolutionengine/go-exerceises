package main

import "fmt"

func main() {
	//Looping through with for & range
	str := "Go is a beautiful language"
	fmt.Printf("The length of str is %d\n", len(str))
	for pos, char := range str {
		fmt.Printf("The character at position %d is: %c\n", pos, char)
	}

	fmt.Println()

	str2 := "Chinese: 日本語"
	fmt.Printf("The length of str2 is %d\n", len(str2))
	for pos, char := range str2 {
		fmt.Printf("The character at position %d is: %c\n", pos, char)
	}
}
