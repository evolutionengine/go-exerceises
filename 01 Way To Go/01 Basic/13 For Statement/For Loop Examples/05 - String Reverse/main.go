package main

import (
	"bufio"
	"fmt"
	"os"
)

func reverse(s string) string {
	runes := []rune(s)
	for i, j := 0, len(runes)-1; i < j; i, j = i+1, j-1 {
		runes[i], runes[j] = runes[j], runes[i]
	}
	return string(runes)
}

func main() {
	//Creating scanner to read entire sentence
	scanner := bufio.NewReader(os.Stdin)
	fmt.Printf("Enter a string: ")
	//Reading input
	str, _ := scanner.ReadString('\n')
	//Printing reverse string
	reverseStr := reverse(str)
	fmt.Println("The reverse string is: ", reverseStr)
	fmt.Printf("", var)
}
