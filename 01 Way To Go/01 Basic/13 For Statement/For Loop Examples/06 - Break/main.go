package main

func main() {
	//Using 'break' in a for loop
	for i := 0; i < 3; i++ {
		for j := 0; j < 10; j++ {
			if j > 5 {
				break
				//break exists the inner most loop
			}
			print(j)
		}
		print(" ")
	}
}
