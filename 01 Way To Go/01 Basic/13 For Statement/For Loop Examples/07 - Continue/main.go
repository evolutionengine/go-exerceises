package main

func main() {
	for i := 0; i < 10; i++ {
		if i == 3 {
			continue
			//continue skips the next part of the loop and continues to next iteration
			//"3" will not be printed
		}
		print(i)
		print(" ")
	}
}
