//Go uses only "For" for creating loops
//There is no do-while or while loops in Go
package main

import "fmt"

func main() {
	//Printing no. from 0 to 5 using for loop
	for i := 0; i <= 5; i++ {
		fmt.Printf("The nos. from 0 to 5 are: %d\n", i)
	}
	fmt.Println()
	//printing characters in string using for loops
	str := "Go is a beautiful language"
	fmt.Printf("Length of the string str is: %d\n", len(str))
	fmt.Println()
	for i := 0; i < len(str); i++ {
		fmt.Printf("The character at position %d is: %c\n", i, str[i])
	}
	fmt.Println()
	//It also works on unicode characters
	str2 := "日本語"
	fmt.Printf("Length of the new string str is: %d\n", len(str2))
	fmt.Println()
	for i := 0; i < len(str2); i++ {
		fmt.Printf("The character at position %d is: %c\n", i, str2[i])
	}
}
