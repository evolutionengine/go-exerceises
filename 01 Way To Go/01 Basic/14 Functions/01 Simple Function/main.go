package main

import "fmt"

func main() {
	var num1 int
	var num2 int
	fmt.Printf("Enter first no: ")
	fmt.Scan(&num1)

	fmt.Printf("Enter second no: ")
	fmt.Scan(&num2)
	//Passing the variables in multiply() and assiging it to new variable
	newNum := multiply(num1, num2)
	fmt.Println("The new no is:", newNum)
}

//multiple() - funciton to multiple 2 variables and return value
func multiply(a int, b int) int {
	return a * b
}
