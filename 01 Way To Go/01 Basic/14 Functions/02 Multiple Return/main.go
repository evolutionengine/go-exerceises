package main

import (
	"fmt"
	//Importing multiple package
	"./multiple"
)

func main() {
	var (
		num1 int
		num2 int
		num3 int
	)
	fmt.Printf("Enter first no: ")
	fmt.Scan(&num1)
	fmt.Printf("Enter second no: ")
	fmt.Scan(&num2)
	fmt.Printf("Enter third no: ")
	fmt.Scan(&num3)

	//For the sake of exercise Multiple() is created in a package
	firstNewNum, secondNewNum := multiple.Multiple(num1, num2, num3)
	fmt.Println("First New No:", firstNewNum)
	fmt.Println("Second New No:", secondNewNum)

}
