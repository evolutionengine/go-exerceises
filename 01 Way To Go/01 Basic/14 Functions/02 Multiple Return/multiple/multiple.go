//Package multiple - Declaring a package "multiple", the package name and folder name has to be same
package multiple

//Multiple - First alphabet of function is in Capital as it has to be exported
//Mutiple() takes 3 parameters and return 2 parameters
func Multiple(a int, b int, c int) (int, int) {
	return a*b + c, a + b/c
}
