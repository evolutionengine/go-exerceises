package main

import "fmt"

func main() {
	//passing multiple int values to the func min()
	x := min(1, 2, 3, 4, 5, 6, 8, 9, 13)
	fmt.Printf("The minimum no is: %d\n", x)

	//passing an array to func min()
	arr := []int{9, 45, 67, 12, 34, 567, 98}
	x = min(arr...)
	fmt.Printf("The min no in the array is: %d\n", x)
}

//min() - receives the multipe values with the below syntax
func min(a ...int) int {
	if len(a) == 0 {
		fmt.Printf("The slice is empty!")
		return 0
	}
	mimNum := a[0]

	for _, i := range a {
		if i < mimNum {
			mimNum = i
		}
	}
	return mimNum
}
