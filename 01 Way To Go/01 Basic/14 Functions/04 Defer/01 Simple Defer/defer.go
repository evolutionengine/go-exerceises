package main

import "fmt"

func main() {
	Function1()
}

func Function1() {
	fmt.Printf("In Function 1 at top\n")
	defer Function2()
	fmt.Printf("In Function 2 at bottom\n")
}

func Function2() {
	fmt.Printf("Function 2 is deferred till the end of Function 1\n")
}
