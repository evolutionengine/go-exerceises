package main

import "fmt"

func main() {
	doDBOperations()
}

func doDBOperations() {
	connectToDB()
	defer disconnectFromDB()
	fmt.Printf("Conducting DB operations.\n")
}

func connectToDB() {
	fmt.Printf("Connected to DB...\n")
}

func disconnectFromDB() {
	fmt.Printf("Diconnected from DB !\n")
}
