package main

import (
	"io"
	"log"
)

func main() {
	func1("Go")
}

func func1(s string) (n int, err error) {
	//note - here we are declaring a self invoking function ! And deferring it.
	defer func() {
		log.Printf("func1(%q) = %d, %v\n", s, n, err)
	}()
	return 7, io.EOF
}
