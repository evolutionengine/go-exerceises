package main

import "fmt"

func main() {
	result := 0

	for i := 0; i < 10; i++ {
		result = fibonacci(i)
		fmt.Printf("Fibonacci(%d) is: %d\n", i, result)
	}
}

//fibonacci - is a recursive function
func fibonacci(n int) (res int) {
	if n <= 1 {
		res = 1
	} else {
		//here fibonacci() is calling itself inside the function
		res = fibonacci(n-1) + fibonacci(n-2)
	}
	return
}
