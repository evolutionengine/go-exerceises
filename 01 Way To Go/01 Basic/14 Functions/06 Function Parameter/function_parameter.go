package main

import "fmt"

//Functions can also be passed as paramters
//This is commonly called as Callback
func main() {
	callback(1, Add)
}

func Add(a, b int) {
	fmt.Printf("The sum of %d and %d is: %d\n", a, b, a+b)
}

func callback(y int, f func(int, int)) {
	f(y, 3) //This translates to Add(1,3)
}
