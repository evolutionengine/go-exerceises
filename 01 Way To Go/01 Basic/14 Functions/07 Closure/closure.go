//Closure is also known as anonymous function or lambda function or function literal
package main

import "fmt"

func main() {
	//Following is a self invoking function
	func(a int) {
		for i := 0; i < a; i++ {
			fmt.Println(a)
		}
	}(6) //Here we are passing the value in the function
	fmt.Println()

	newInt := intSeq()

	for i := 0; i < 4; i++ {
		fmt.Println(newInt())
	}
	fmt.Println()

	nextInt := intSeq()
	for i := 0; i < 2; i++ {
		fmt.Println(nextInt())
	}
}

//intSeq - is returning a anonymous function
func intSeq() func() int {
	i := 0
	return func() int {
		i++
		return i
	}
}
