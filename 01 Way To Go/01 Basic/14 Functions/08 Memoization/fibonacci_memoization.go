package main

import (
	"fmt"
	"time"
)

//LIM - limiting iterations to 41
const LIM = 41

//Declaring an array for memoization
var fibs [LIM]uint64

func main() {
	var result uint64
	//Recording start time for comparision
	start := time.Now()
	for i := 0; i < LIM; i++ {
		result = fibonacci(i)
		fmt.Printf("fibonacci(%d) is: %d\n", i, result)
	}
	//Recording stop time of operation
	end := time.Now()
	//Calculating time taken
	delta := end.Sub(start)
	fmt.Println()

	fmt.Printf("Calculation took: %s\n", delta)
}

func fibonacci(n int) (res uint64) {
	//Memoization: checking for values in array
	if fibs[n] != 0 {
		res = fibs[n]
		return
	}
	//Checking & creating fibonacci numbers
	if n <= 1 {
		res = 1
	} else {
		res = fibonacci(n-1) + fibonacci(n-2)
	}
	//Recording values in fibs[] for memoization
	fibs[n] = res
	return
}
