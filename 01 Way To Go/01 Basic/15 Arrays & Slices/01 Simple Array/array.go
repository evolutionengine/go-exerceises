package main

import "fmt"

func main() {
	//Declaring array of size 10
	//Array can contain only one data type
	var arr [10]int
	//Looping through array and printing values
	for i := 0; i < len(arr); i++ {
		arr[i] = i + 2
		fmt.Printf("The value at position %d in array is: %d\n", i, arr[i])
	}
}
