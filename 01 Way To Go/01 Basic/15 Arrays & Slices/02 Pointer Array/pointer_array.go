package main

import "fmt"

func main() {
	//Declaring an int type array and assigning values
	ar := [3]int{2, 4, 6}
	fmt.Println()
	//Passing the array to a regular function
	fmt.Println("The values in the array in the f(ar) are:")
	f(ar)
	//Printing and checking the values in original array
	fmt.Println()
	fmt.Printf("After f(ar) the values in the original array are: %v", ar)
	fmt.Println()
	fmt.Println()
	//Printing and checking the values in original array after passing it to pointer function
	fp(&ar)
	//Checking the values in original array
	fmt.Println()
	fmt.Printf("Now the values in original array are: ")
	fmt.Printf("%v", ar)
	fmt.Println()
	fmt.Println()
}

//Passing values to a regular function creates a copy of the values
func f(a [3]int) {
	for i := 0; i < len(a); i++ {
		a[i] = a[i] * 2
		fmt.Printf("The values at position %d is: %d\n", i, a[i])
	}
}

//Pointer functions gives the memory address and changes the value directly
func fp(a *[3]int) {
	fmt.Println("The values in f(&ar) are:")
	for i := 0; i < len(a); i++ {
		a[i] = a[i] * 2
		fmt.Printf("The values at position %d is: %d\n", i, a[i])
	}
}
