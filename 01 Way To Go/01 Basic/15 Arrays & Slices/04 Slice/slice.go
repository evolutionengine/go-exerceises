//Slices in lay man terms is an array without the fixed limit
//Slices are very flexible and are references so take less memory and more efficient than array
package main

import "fmt"

//Capacity of a slice is equal to the length of the underlying array
func main() {
	//Declaration
	//var slice []int
	//if var slice []int = arr1[:]
	//Means the slice is equal to the complete array arr1, it can also be declared as slice = &arr1
	//var slice []int = arr1[2:] -> Means slice contains all elements from 2nd index to last
	//var slice []int = arr1[:3] -> Slice contains all elements from 1st index to (not including) 3rd index

	var arr1 [6]int
	slice := arr1[2:5]

	//Loading the array with ints
	for i := 0; i < len(arr1); i++ {
		arr1[i] = i
	}

	//Printing the slice
	for i := 0; i < len(slice); i++ {
		fmt.Printf("The slice at position %d is: %d\n", i, slice[i])
	}
	fmt.Println("")
	fmt.Printf("The length of array is: %d\n", len(arr1))
	fmt.Printf("The length of slice is: %d\n", len(slice))
	fmt.Printf("The capacity of slice is: %d\n", cap(slice))
	fmt.Println("")

	//Growing the slice
	slice = arr1[0:4]
	for i := 0; i < len(slice); i++ {
		fmt.Printf("The slice at position %d is: %d\n", i, slice[i])
	}
	fmt.Println("")
	fmt.Printf("The length of slice is: %d\n", len(slice))
	fmt.Printf("The capacity of slice is: %d\n", cap(slice))
	fmt.Println("")
}
