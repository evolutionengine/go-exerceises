//Often slice and underlying array can be created together using make()
package main

import "fmt"

func main() {
	//Slice & underlying array can be declared as slice := make([]type, length)
	//s2 := make([]int, 20) => cap(s2) = len(s2) = 10
	//Slice can also be created with specific length, slice := make([]type, len, cap)
	// s3 := make([]int, 5, 10) , it can also be declared as => new([10]int)[0:5] , new() returns a pointer

	//Declaring slice
	slice1 := make([]int, 10)
	//Loading the slice with data
	for i := 0; i < len(slice1); i++ {
		slice1[i] = 5 * i
		fmt.Printf("Slice at %d is: %d\n", i, slice1[i])
	}
	fmt.Println("")
	fmt.Printf("The len of slice is: %d\n", len(slice1))
	fmt.Printf("The cap of slice is: %d\n", cap(slice1))
	fmt.Println("")
}
