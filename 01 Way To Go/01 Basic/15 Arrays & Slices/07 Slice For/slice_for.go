package main

import "fmt"

func main() {
	slice1 := make([]int, 10)

	//loading the slice
	for i := 0; i < len(slice1); i++ {
		slice1[i] = i + 6
	}

	//Using for-range to print the values
	for p, v := range slice1 {
		fmt.Printf("The slice at position %d is: %d\n", p, v)
	}
	fmt.Println()
	//Example - String Slice
	sliceString := []string{"Jan", "Feb", "Mar", "Apr", "May"}
	for p, v := range sliceString {
		fmt.Printf("The slice at position %v is: %v\n", p, v)
	}
}
