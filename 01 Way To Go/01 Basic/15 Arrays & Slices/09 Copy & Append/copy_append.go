package main

import "fmt"

func main() {
	s1_from := []int{1, 2, 3}
	s2_to := make([]int, 10)

	n := copy(s2_to, s1_from)
	fmt.Println(s2_to)
	fmt.Printf("Copied %d elements from s1_from\n", n)
	fmt.Println()

	s3 := []int{10, 9, 8}
	s3 = append(s3, 7, 6, 5)
	fmt.Println(s3)

}
