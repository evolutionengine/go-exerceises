package main

import "fmt"

func main() {
	//Converting string to an byte array
	//Individual characters can be edited in an array
	string1 := "Hello Go!"
	//COnverting to a byte array for character manipulation
	char := []byte(string1)
	//Notes the '' => For assigning byte into the array
	char[0] = 'C'
	string2 := string(char)
	fmt.Println(string2)
	fmt.Printf("String[0] is: %c\n", string1[0])
}
