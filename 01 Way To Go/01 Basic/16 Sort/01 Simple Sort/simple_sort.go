package main

import (
	"fmt"
	"sort"
)

func main() {
	str := []string{"c", "a", "b"}
	//sort.Strings() => Buil-in Function
	sort.Strings(str)
	fmt.Println("Strings:", str)
	fmt.Println("")

	ints := []int{4, 6, 8, 12, 14}
	sort.Ints(ints)
	fmt.Println("Ints:", ints)
	fmt.Println("")

	s := sort.IntsAreSorted(ints)
	fmt.Println("Sorted:", s)
	fmt.Println("")
}
