package main

import (
	"fmt"
	"sort"
)

//Person : type struct, contains Name & Age
type Person struct {
	Name string
	Age  int
}

func (p Person) String() string {
	return fmt.Sprintf("%s: %d", p.Name, p.Age)
}

// ByAge implements sort.Interface for []Person based on
// the Age field.
type ByAge []Person

//sort.Sort(data Interface) => Calls data.Len, data.Less & data.Swap for executing operation
//Sorting by interface is less stable
func (a ByAge) Len() int {
	return len(a)
}

func (a ByAge) Swap(i, j int) {
	a[i], a[j] = a[j], a[i]
}

func (a ByAge) Less(i, j int) bool {
	return a[i].Age < a[j].Age
}

func main() {
	people := []Person{
		{"Bob", 31},
		{"John", 42},
		{"Michael", 17},
		{"Jenny", 26},
	}

	people2 := []Person{
		{"Monster", 101},
		{"Mavis", 118},
		{"Drac", 1000},
		{"Rob", 120},
	}

	fmt.Println(people)
	sort.Sort(ByAge(people))
	fmt.Println(people)
	fmt.Println("")
	//sort.Stable(data Interface) => Better way to sort data Interface
	sort.Stable(ByAge(people2))
	fmt.Println(people2)

}
