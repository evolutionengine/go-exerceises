//Maps work in key: value format
package main

import "fmt"

//for searching where performance matters always use slice, maps are 100X slower
func main() {

	var mapLit map[string]int
	var mapAssigned map[string]int

	mapLit = map[string]int{"one": 1, "two": 2, "three": 3}

	//maps are reference types, memory is allocated by make()
	//maps grow dynamically to accomodate new values
	mapCreated := make(map[string]float32)
	mapAssigned = mapLit

	mapCreated["key1"] = 4.5
	mapCreated["key2"] = 3.14159
	mapAssigned["two"] = 3

	fmt.Println(mapLit)
	fmt.Println(mapCreated)
	fmt.Println(mapAssigned)

	fmt.Printf("The value in 'key 1' in mapCreated is: %f\n", mapCreated["key1"])

	// val, isPresent := mapLit["three"] => To check values in map
}
