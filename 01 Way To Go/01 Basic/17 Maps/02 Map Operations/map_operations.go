package main

import "fmt"

func main() {

	map1 := make(map[string]int)
	map1["New Delhi"] = 01
	map1["London"] = 02
	map1["Tokyo"] = 03
	map1["New York"] = 13

	//Using for - range construct
	for key, valueOf := range map1 {
		fmt.Printf("The key is: %s and the value is: %d\n", key, valueOf)
	}
	fmt.Println("")
	//Using the return signature of map
	value, isPresent := map1["Tokyo"]
	if isPresent {
		fmt.Printf("The value of 'Tokyo' is: %d\n", value)
	} else {
		fmt.Printf("'Tokyo' is not present in the map\n")
	}
	fmt.Println("")

	value, isPresent = map1["Mumbai"]
	if isPresent {
		fmt.Printf("The value of 'Mumbai' is: %d\n", value)
	} else {
		fmt.Printf("'Mumbai' is not present in the map\n")
	}
	fmt.Println("")
	//Deleting a key & value
	delete(map1, "London")
	value, isPresent = map1["London"]
	if isPresent {
		fmt.Printf("The value of 'London' is: %d\n", value)
	} else {
		fmt.Printf("'London' is not present in the map\n")
	}
}
