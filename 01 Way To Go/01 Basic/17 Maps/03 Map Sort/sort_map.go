package main

import (
	"fmt"
	"sort"
)

var (
	barVal = map[string]int{"alpa": 10, "tango": 20, "charlie": 101, "gitta": 89, "peta": 533, "zeta": 984}
)

func main() {
	fmt.Println("Unsorted Map:")
	for key, value := range barVal {
		fmt.Printf("Key[%s] : Value[%d]\n", key, value)
	}
	fmt.Println("")

	//Sorting according to the keys
	//extracting the keys for Sorting
	keysToSort := make([]string, len(barVal))
	i := 0
	for k := range barVal {
		keysToSort[i] = k
		i++
	}

	//Sorting the Map
	sort.Strings(keysToSort)
	fmt.Println("")
	fmt.Println("Sorted Map:")

	for _, val := range keysToSort {
		fmt.Printf("Key[%s] : Value[%d]\n", val, barVal[val])
	}

	//Inverting values
	invMap := make(map[int]string, len(barVal))
	for k, v := range barVal {
		invMap[v] = k
	}
	fmt.Println("Inverted Map:")

	for k, v := range invMap {
		fmt.Printf("Key[%v] : Value[%v]\n", k, v)
	}
}
