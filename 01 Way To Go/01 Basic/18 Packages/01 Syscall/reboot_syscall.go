package main

//syscall - package in std lib for accessing lower level functions
import "syscall"

//program to reboot a linux machine, run in sudo
const (
	LINUX_REBOOT_MAGIC1      = 0xfee1dead
	LINUX_REBOOT_MAGIC2      = 0x28121969
	LINUX_REBOOT_CMD_RESTART = 0x1234567
)

func main() {
	syscall.Syscall(
		syscall.SYS_REBOOT,
		LINUX_REBOOT_MAGIC1,
		LINUX_REBOOT_MAGIC2,
		LINUX_REBOOT_CMD_RESTART,
	)
}
