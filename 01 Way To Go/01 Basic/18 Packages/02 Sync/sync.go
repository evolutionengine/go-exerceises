//Package sync - demo using sync package from Std Lib
//When applications are running concurrently or simulteanously, its important to
//lock the value of shared variables as they are updated, otherwise the values become unpredictable
//The classic approach is to alow a single thread to change a variable at a time
//This is achieved by using sync package => synchronized
//sync.Mutex => mutual exclusion lock , serves as a guard
package sync

//importing sync package
import "sync"

//Info - e.g struct for demo of sync.Mutex
type Info struct {
	mu  sync.Mutex
	Str string
}

//Update - function which make change in the Critical section
func Update(info *Info) {
	info.mu.Lock()
	//Critical section
	info.Str = "New Value"
	//End Critical section
	info.mu.Unlock()
}
