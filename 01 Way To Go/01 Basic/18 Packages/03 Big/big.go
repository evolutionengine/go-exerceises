package main

import (
	"fmt"
	"math"
	"math/big"
)

//floats give accurate numbers upto 15 digits, for more accurate calculations Go provides Big package
func main() {
	im := big.NewInt(math.MaxInt64)
	in := im
	io := big.NewInt(1954)
	ip := big.NewInt(1)

	ip.Mul(im, in).Add(ip, im).Div(ip, io).Sub(im, io)
	fmt.Println("New Big Int:", ip)
}
