package main

import "fmt"

type Contact struct {
	firstName string
	lastName  string
	age       int
	telephone int
	address   string
	birthday  string
}

func main() {
	newContact := new(Contact)
	newContact.firstName = "Anil"
	newContact.lastName = "Kulkarni"
	newContact.age = 33
	newContact.telephone = 9702990997

	newContact2 := new(Contact)
	newContact2.firstName = "Rob"
	newContact2.lastName = "Octy"
	newContact2.age = 1
	newContact2.address = "Internet"

	newContact3 := Contact{"Teja", "Kulkarni", 31, 99999999, "New York", "13/05/86"}
	fmt.Printf(
		"Hello, %s %s! \tYour Age is: %d \tYour Contact No is: %d\n",
		newContact.firstName, newContact.lastName, newContact.age, newContact.telephone)
	fmt.Println(newContact)
	fmt.Println(newContact2)
	fmt.Println(newContact3)
}
