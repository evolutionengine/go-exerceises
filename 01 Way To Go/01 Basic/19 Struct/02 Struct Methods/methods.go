package main

import (
	"fmt"
	"strings"
)

//Person : type Person defining First & Last Name
type Person struct {
	firstName string
	lastName  string
}

func upPerson(p *Person) {
	p.firstName = strings.ToUpper(p.firstName)
	p.lastName = strings.ToUpper(p.lastName)
}

func toLowerCase(p *Person) {
	p.firstName = strings.ToLower(p.firstName)
	p.lastName = strings.ToLower(p.lastName)
}

func main() {
	//Struct as a value type
	var persn1 Person
	persn1.firstName = "Johnny"
	persn1.lastName = "Cigar"
	upPerson(&persn1)
	fmt.Printf("Hi %s %s\n", persn1.firstName, persn1.lastName)

	//Struct as a pointer
	persn2 := new(Person)
	persn2.firstName = "Hugo"
	persn2.lastName = "Dothraki"
	toLowerCase(persn2)
	fmt.Printf("Hello, %s %s\n", persn2.firstName, persn2.lastName)

	//Struct as a literal
	persn3 := &Person{"John", "Snow"}
	upPerson(persn3)
	fmt.Printf("Hi, %s %s\n", persn3.firstName, persn3.lastName)
}
