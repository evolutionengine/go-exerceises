package main

import "fmt"

type innerS struct {
	in1 int
	in2 int
}

type outerS struct {
	b      int
	c      float32
	int    //Anonymous field
	innerS //Anonymous field
}

func main() {
	outer := outerS{4, 5.67, 10, innerS{7, 9}}
	fmt.Println(outer)

	fmt.Printf("The outerS b is: %v\n", outer.b)
	fmt.Printf("The outerS c is: %v\n", outer.c)
	fmt.Printf("The outerS anony int is: %v\n", outer.int)
	fmt.Printf("The outer in1 is: %v\n", outer.in1)
	fmt.Printf("The outer in2 is: %v\n", outer.in2)
}
