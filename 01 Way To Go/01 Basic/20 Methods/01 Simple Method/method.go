package main

import "fmt"

//TwoInts :
type TwoInts struct {
	a int
	b int
}

func main() {
	two1 := TwoInts{4, 5}

	fmt.Printf("The sum of the nos is: %v\n", two1.AddThem())
	fmt.Printf("Add them to param: %v\n", two1.AddToParam(20))
}

// Basic structure of method is - func (receiver type) Name(paramters) (return type)

//AddThem :
func (tn *TwoInts) AddThem() int {
	return tn.a + tn.b
}

//AddToParam :
func (tn *TwoInts) AddToParam(param int) int {
	return tn.a + param + tn.b
}
