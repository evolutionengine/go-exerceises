package main

import (
	"fmt"
	"time"
)

type myTime struct {
	time.Time //Declaring anonymous field
}

func main() {
	m := myTime{time.Now()}
	//Calling existing String() on anonymous field
	fmt.Printf("The full time is: %v\n", m.String())
	//Calling first3Chars method
	fmt.Printf("The first three char: %v\n", m.first3Chars())
}

//methods can have access to functions declared within the same package only

//first3Chars : Returns first 3 chars of time

func (t myTime) first3Chars() string {
	return t.Time.String()[0:3]
}
