package main

import (
	"fmt"

	"./person"
)

func main() {
	p := new(person.Person)
	p.SetFirstName("Robert")
	fmt.Println(p.FirstName())
}
