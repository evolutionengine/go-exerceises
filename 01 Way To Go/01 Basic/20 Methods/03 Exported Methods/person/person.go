package person

//Person : type struct - Defining Person
type Person struct {
	firstName string
	lastName  string
}

//FirstName : Exported function
func (p *Person) FirstName() string {
	return p.firstName
}

//SetFirstName : Sets the first name
func (p *Person) SetFirstName(newName string) {
	p.firstName = newName
}
