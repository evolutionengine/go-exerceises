package main

import (
	"fmt"
	"math"
)

//Point :
type Point struct {
	x, y float64
}

//Abs :
func (p *Point) Abs() float64 {
	return math.Sqrt(p.x*p.x + p.y*p.y)
}

//NamedPoint :
type NamedPoint struct {
	Point //embedded struct
	name  string
}

func main() {
	p := &NamedPoint{Point{3, 4}, "Pythagoras"}
	fmt.Println(p.Abs())

}
