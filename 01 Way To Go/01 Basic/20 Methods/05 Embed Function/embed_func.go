package main

import "fmt"

//Log :
type Log struct {
	msg string
}

//Customer :
type Customer struct {
	name string
	Log  //Anonymous embed
}

//Add :
func (l *Log) Add(m string) {
	l.msg += "\n" + m
}

func main() {
	cust := &Customer{"Markus Indefenia", Log{"01) World is my playground!"}}
	cust.Add("02) I am the master & also the puppet!")
	fmt.Println("Name of customer:", cust.name, "His message is:", cust.msg)
}
