package main

import "fmt"

type Camera struct{}

func (c *Camera) TakePicture() string {
	return "Click!"
}

type Phone struct{}

func (c *Camera) call() string {
	return "Tring Tring"
}

//CameraPhone : Struct with multiple inheritance
type CameraPhone struct {
	Camera
	Phone
}

func main() {
	cp := new(CameraPhone)
	fmt.Println("Our Phone can do lot of things!")
	fmt.Println("Like taking a picture: ", cp.TakePicture())
	fmt.Println("It can also place a call:", cp.call())
}
