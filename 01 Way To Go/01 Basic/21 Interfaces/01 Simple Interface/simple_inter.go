package main

import "fmt"

//Go interfaces can have 0 to max 3 methods

//Shaper : Type interface
type Shaper interface {
	Area() float32
}

type Square struct {
	side float32
}

func main() {
	sq := &Square{5.00}
	areaSquare := sq
	fmt.Println("The area of square is:", areaSquare.Area())
}

func (s *Square) Area() float32 {
	return s.side * s.side
}
