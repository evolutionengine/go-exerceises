package main

import (
	"fmt"
	"math"
)

//Shaper :
type Shaper interface {
	Area() float32
}

//Square :
type Square struct {
	side float32
}

//Area :
func (sq *Square) Area() float32 {
	return sq.side * sq.side
}

//Rectangle :
type Rectangle struct {
	length float32
	width  float32
}

//Area :
func (r *Rectangle) Area() float32 {
	return r.length * r.width
}

//Circle :
type Circle struct {
	radius float32
}

//Area :
func (c *Circle) Area() float32 {

	//Changing float64 of math.Pi to float32 for matching return type
	p := float32(math.Pi)
	return 2 * p * c.radius
}

func main() {
	r := &Rectangle{5, 10}
	s := &Square{8}
	c := &Circle{23}

	//shapes : Array of Interface Shaper
	shapes := []Shaper{r, s, c}
	fmt.Println("Looping through shapes for area...")

	//Loop through Shaper and print the values
	for key := range shapes {
		fmt.Println("Shape details:", shapes[key])
		fmt.Println("Area of the shape is:", shapes[key].Area())
	}
}
