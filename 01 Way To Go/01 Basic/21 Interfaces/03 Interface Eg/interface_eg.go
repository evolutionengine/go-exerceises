package main

import "fmt"

type stockPosition struct {
	ticker     string
	sharePrice float32
	count      float32
}

//method to determine the value of stock price
func (s *stockPosition) getValue() float32 {
	return s.sharePrice * s.count
}

type car struct {
	make  string
	model string
	price float32
}

//method to get price of the car
func (c *car) getValue() float32 {
	return c.price
}

//Defining valuable interface
type valuable interface {
	getValue() float32
}

//anything that accepts "valuable" is accepted
func showValue(asset valuable) {
	fmt.Println("The value of the asset is:", asset.getValue())
}

func main() {
	stock := &stockPosition{"Octa", 794.46, 246.32}
	showValue(stock)

	newCar := &car{"Rolls Royce", "Ghost", 26579000}
	showValue(newCar)
}
