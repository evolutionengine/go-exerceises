package main

import (
	"fmt"
	"math"
)

type Shaper interface {
	Area() float32
}

type square struct {
	side float32
}

type rectangle struct {
	length float32
	width  float32
}

type circle struct {
	radius float32
}

func (sq *square) Area() float32 {
	return sq.side * sq.side
}

func (re *rectangle) Area() float32 {
	return re.width * re.length
}

func (ci *circle) Area() float32 {
	return 2 * math.Pi * ci.radius
}

func main() {
	sq := &square{5}
	var areaIntF Shaper
	areaIntF = sq

	//Checking the interface type for square
	if t, ok := areaIntF.(*square); ok {
		fmt.Printf("The type of interface is: %T\n", t)
	}

	//Checking the interface type for circle
	if u, ok := areaIntF.(*circle); ok {
		fmt.Printf("The type of interface is: %T\n", u)
	} else {
		fmt.Printf("areaIntF does not contain variable of type 'Circle' !\n")
	}

	//Testing the variable type using switch
	switch t := areaIntF.(type) {
	case *square:
		fmt.Printf("Type Square %T with value of %v\n", t, t)
	case *circle:
		fmt.Printf("Type Circle %T with value of %v\n", t, t)
	case *rectangle:
		fmt.Printf("Type rectangle %T with value of %v\n", t, t)
	case nil:
		fmt.Printf("Type nil, nothing to check!\n")
	default:
		fmt.Printf("Unexpected type %T", t)
	}
}
