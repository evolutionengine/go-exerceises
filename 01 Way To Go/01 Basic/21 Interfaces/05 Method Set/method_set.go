//Program Not Working
package main

import "fmt"

type List []int

func (l List) Len() int {
	return len(l)
}

func (l *List) Append(val int) {
	*l = append(*l, val)
}

type Appender interface {
	append(List) int
}

func CountInto(a Appender, start, end int) {
	for i := start; i <= end; i++ {
		a.append(List, i)
	}
}

type Lener interface {
	Len() int
}

func LongEnough(l Lener) bool {
	return l.Len()*10 < 42
}

func main() {
	var lst List
	CountInto(lst, 1, 10)
	if LongEnough(lst) {
		fmt.Println("- List is long enough!")

		plst := new(List)

		CountInto(plst, 1, 10)

		if LongEnough(plst) {
			fmt.Println(" - List is long enough")
		}
	}
}
