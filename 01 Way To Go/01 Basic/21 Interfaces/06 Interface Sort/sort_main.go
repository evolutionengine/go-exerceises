//Program Not Working
package main

import (
	"fmt"

	"./sort"
	"reflect"
)

func ints() {
	//creating a slice of int
	data := []int{0, 3, 64, 64, -31, -42, 43543, 42, 4353, 54634, -533, 12, 97, 9437}
	//convert the int slice into type IntArray []int, it implements Sorter Interface via Len, Swap & Less methods
	a := sort.IntArray(data)
	//sort.Sort(a) accepts 'a' as it implements Sorter Interface, other data types would not be accepted
	sort.Sort(a)
	fmt.Println(reflect.TypeOf(a).Name())
	if !sort.IsSorted(a) {
		panic("fail")
	}

	fmt.Printf("The sorted array is %v\n", a)
}

func strings() {
	data := []string{"sunday", "tuesday", "monday", "saturday", "", "friday", "wednesday", "thrusday"}
	a := sort.StringArray(data)
	sort.Sort(a)
}

type day struct {
	num       int
	shortName string
	longName  string
}

type dayArray struct {
	data []*day
}

func (p *dayArray) Len() int {
	return len(p.data)
}

func (p *dayArray) Less(i, j int) bool {
	return p.data[i].num < p.data[j].num
}

func (p *dayArray) Swap(i, j int)  {
	p.data[i], p.data[j] = p.data[j], p.data[i]
}

func days()  {
	sunday := day{0,"Sun", "Sunday"}
	monday := day{1, "Mon", "Monday"}
	tuesay := day{2, "Tue", "Tuesday"}
	wednesday := day{3, "Wed", "Wednesday"}
	thrusday := day{4,"Thr", "Thrusday"}
	friday := day{5, "Fri", "Friday"}
	saturday := day{6,"Sat", "Saturday"}

	data := []*day{&monday, &saturday, &sunday, &wednesday, &friday, &thrusday, &tuesay}
	a := dayArray{data}
	sort.Sort(&a)

	if !sort.IsSorted(&a) {
		panic("fail")
	}

	for _, v := range data {
		fmt.Printf("%s\n", v.longName)
	}
	fmt.Printf("\n")
}

func main() {
	ints()
	days()
	strings()
}

//Needs Investigation

/*
Interfaces contain specific methods, generally not more than 3
These methods are said to implement the interface

Len(), Swap() & Less() are defined in type Sorter interface and have receiver signature of type intArray []int
Thus intArray also implements Sorter interface when used in the methods -> Less, Swap & Len

intArray holds the data that needs to be sorted
Sort() => has a receiver parameter of (name Sorter)

As Sort() only accepts type Sorter, the input data has to be converted which implements Sorter, a := sort.IntArray(data)
intArray implements Sorter via Less, Swap & Len
it serves also as a check if the input data can be sorted or not as it won't accept data types that do not
implement Sorter Interface

Then actual sorting happens via sort.Sort(a)
*/

/*
Steps for implementing Sort()
1) Create data that needs to be sorted
2) Convert data into a type which implements Sorter Interface so that we get access to Sort()
3) Pass the data to Sort() method via sort.Sort(convertedData)
 */

 /*
 2.1) Data type can implement Sorter via methods that are defined in the Sorter
 2.2) Pass the data type in methods defined in Sorter (Len, Less & Swap), so that it implements the interface Sorter
  */

  /*
  1) Interface is a mechanism to make the code cleaner
  2) It groups together methods that can be reused by other functions
  3) In order to reuse, just convert the data type to interface type and pass methods
  4) In short interfaces are mechanisms to group together reusable methods
   */