package main

import "fmt"

type Person struct {
	name string
	age int
}

type Any interface {}

func main() {
	i:= 5
	str := "ABC"
	//val implements the empty interface Any
	var val Any
	val = i
	fmt.Printf("Val the value of %v\n", val)

	val = str
	fmt.Printf("Val the value of %v\n", val)

	person1 := &Person{"Rob Pike", 55}
	val = person1
	fmt.Printf("Val the value of %v\n", val)

	switch t := val.(type) {
	case int:
		fmt.Printf("The type is %T\n", t)
	case string:
		fmt.Printf("The type is %T\n", t)
	case *Person:
		fmt.Printf("The type is %T\n", t)
	case Person:
		fmt.Printf("The type is %T\n", t)
	default:
		fmt.Printf("Unexpected type %T\n", t)
	}
}
