package main

import (
	"fmt"
	"reflect"
)

type Sayer interface {
	Say() string
}

type Cat struct{}

func (c Cat) Say() string { return "meow" }

type Dog struct{}

func (d Dog) Say() string { return "woof" }

type Horse struct{}

func (h Horse) Say() string { return "neigh" }

func main() {
	c := Cat{}
	fmt.Println("Cat says:", c.Say())
	d := Dog{}
	fmt.Println("Dog says:", d.Say())

	animals := []Sayer{c, d}
	for _, a := range animals {
		fmt.Println(reflect.TypeOf(a).Name(), "says:", a.Say())
	}

	animals = append(animals, Horse{})
	for _, a := range animals {
		fmt.Println(reflect.TypeOf(a).Name(), "says:", a.Say())
	}

	MakeCatTalk(c)
	MakeTalk(animals[2])
}

func MakeCatTalk(c Cat) {
	fmt.Println("Cat says:", c.Say())
}

func MakeTalk(s Sayer) {
	fmt.Println(reflect.TypeOf(s).Name(), "says:", s.Say())
}
