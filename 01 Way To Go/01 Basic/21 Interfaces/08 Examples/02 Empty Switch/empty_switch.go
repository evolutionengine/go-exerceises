package main

import (
	"fmt"
	"reflect"
)

// Empty interface
type inputData interface{}

// TypeSwitch () accepts data of inputData type and switches according to the type
func TypeSwitch(data inputData) {
	switch data.(type) {
	case string:
		fmt.Println("The type of data is:", reflect.TypeOf(data).Name(), "and the data is:", data)
	case int:
		fmt.Println("The type of data is:", reflect.TypeOf(data).Name(), "and the data is:", data)
	case bool:
		fmt.Println("The type of data is:", reflect.TypeOf(data).Name(), "and the data is:", data)
	default:
		fmt.Printf("Data type of %T is not defined\n", data)
	}
}

func main() {
	var typeString inputData = "Hi I'am a String"
	input := typeString
	TypeSwitch(input)

	var typeInt inputData = 1234
	input = typeInt
	TypeSwitch(input)

	var typeBool inputData = true
	input = typeBool
	TypeSwitch(input)
}
