package main

import (
	"fmt"
)

type Node struct {
	le   *Node
	data interface{}
	ri   *Node
}

func NewNode(left, right *Node) *Node {
	return &Node{left, nil, right}
}

func (n *Node) setData(data interface{}) {
	n.data = data
}

func main() {
	root := NewNode(nil, nil)
	root.setData("Root-Node")

	a := NewNode(nil, nil)
	a.setData("Left Node")

	b := NewNode(nil, nil)
	b.setData("Right Node")

	root.le = a
	root.ri = b

	fmt.Printf("The root node is: %v\n", root)
}
