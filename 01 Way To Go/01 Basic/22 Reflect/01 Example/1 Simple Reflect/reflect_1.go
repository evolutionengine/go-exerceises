package __Simple_Reflect

import (
	"fmt"
	"reflect"
)

func main() {
	var x float64 = 3.44
	fmt.Println("Type:", reflect.TypeOf(x))
	v := reflect.ValueOf(x)
	fmt.Println("Value:", v)
	fmt.Println("Type:", v.Type())
	fmt.Println("Kind:", v.Kind())
	fmt.Println("Value:", v.Float())
	fmt.Println(v.Interface())
	fmt.Printf("Value of %5.2e\n", v.Interface())
	y := v.Interface().(float64)
	fmt.Println(y)
}
