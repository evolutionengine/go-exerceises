package main

import (
	"fmt"
	"reflect"
)

func main()  {
	x := 3.4
	v := reflect.ValueOf(x)
	//Setting the value of x
	fmt.Println("Can 'v' be set:", v.CanSet())
	//Reflection requires the address in order to change the values
	v = reflect.ValueOf(&x)
	fmt.Println("Type of v:", v.Type())
	fmt.Println("Can 'v' be set:", v.CanSet())
	//Making v settable using Elem()
	v = v.Elem()
	fmt.Println("The Elem of v is:", v)
	fmt.Println("Can 'v' be set:",v.CanSet())
	v.SetFloat(3.1415)
	fmt.Println(v.Interface())
	fmt.Println(v)
}
