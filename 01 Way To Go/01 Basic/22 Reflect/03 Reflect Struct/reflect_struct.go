package main

import (
	"fmt"
	"reflect"
)

type NotKnownType struct {
	s1, s2, s3 string
}

func (s NotKnownType) String() string {
	return s.s1 + "-" + s.s2 + "-" + s.s3
}

var secret interface{} = NotKnownType{"Ada", "Go","Oberon"}

func main() {
	value := reflect.ValueOf(secret)
	typ := reflect.TypeOf(secret)

	fmt.Println(typ)

	knd := value.Kind()
	fmt.Println(knd)

	//iterating through fields in struct
	for i := 0; i < value.NumField(); i++ {
		fmt.Printf("Field %v: %v\n", i, value.Field(i))
	}

	//calling the first method
	results := value.Method(0).Call(nil)
	fmt.Println(results)
}
