package main

import (
	"fmt"
	"bufio"
	"os"
)
//Package bufio gives buffered input & output
//It also requires package os for reading from the std input

func main() {
	//create a new reader
	inputReader := bufio.NewReader(os.Stdin)
	fmt.Printf("Greetings from Mavis, how are you feeling today ? ")
	//reads the line till the end of the line using a delim \n
	input, err := inputReader.ReadString('\n')
	//checking for any errors & then printing
	if err == nil {
		fmt.Printf("You are feeling: %s\n", input)
	} else {
		fmt.Println("There was some error reading the input, exiting program...")
		return
	}
}
