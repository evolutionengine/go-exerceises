package main

import (
	"fmt"
	"os"
	"bufio"
	"io"
)

func main() {
	inputFile, inputError := os.Open("text.md")
	if inputError != nil {
		fmt.Printf("There was an error opening the input file\n" +
							"Do you have the permissions to read it ?")
		return
	}
	defer inputFile.Close()

	inputReader := bufio.NewReader(inputFile)

	for {
		inputString, readerError := inputReader.ReadString('\n')
		if readerError == io.EOF {
			return
		}
		fmt.Printf("The input was: %s", inputString)
	}
}
