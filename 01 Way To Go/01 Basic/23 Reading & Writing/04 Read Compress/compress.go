package main

import (
	"fmt"
	"os"
	"compress/gzip"
	"bufio"
)

func main() {
	var r *bufio.Reader
	fName := "file.gz"
	fi, err := os.Open(fName)
	if err != nil {
		fmt.Fprint(os.Stderr, "%v can't open %s: error %s\n", os.Args[0], fName, err)
		os.Exit(1)
	}

	fz, err := gzip.NewReader(fi)
	if err != nil {
		r = bufio.NewReader(fi)
	} else {
		r = bufio.NewReader(fz)
	}

	for {
		line, err := r.ReadString('\n')
		if err != nil {
			fmt.Println("Done reading file")
			os.Exit(0)
		}
		fmt.Println(line)
	}
}
