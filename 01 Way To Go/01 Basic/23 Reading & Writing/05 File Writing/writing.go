package main

import (
	"fmt"
	"os"
	"bufio"
)

func main() {
	outputFile, outputError := os.OpenFile("output.dat", os.O_WRONLY|os.O_CREATE, 0666)
	if outputError != nil {
		fmt.Printf("An error occured while creating the file\n")
		return
	}
	defer outputFile.Close()
	//Create a new writer
	outputWriter := bufio.NewWriter(outputFile)
	outputString := "Hello, World!\n"

	for i := 0; i < 10; i++ {
		outputWriter.WriteString(outputString)
	}
	outputWriter.Flush()
}

//Code working, code spitted output.dat with the right data
