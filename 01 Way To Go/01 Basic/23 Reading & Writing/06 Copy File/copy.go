package main

import (
	"fmt"
	"os"
	"io"
)

func CopyFile(dstName, srcName string) (written int64, err error) {
	src, err := os.Open(srcName)
	if err != nil{
		fmt.Printf("Error Encountered: %s\n", err)
		return
	}
	defer src.Close()

	dst, err := os.OpenFile(dstName, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		fmt.Printf("Error Encountered: %s\n", err)
		return
	}
	defer dst.Close()

	return io.Copy(dst, src)
}

func main() {
	CopyFile("target.txt", "source.txt")
	fmt.Println("Copy Done!")
}
