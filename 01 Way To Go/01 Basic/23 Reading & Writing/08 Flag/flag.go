package main

import (
	"flag"
	"os"
)
//Flags are often used in command line applications as arguments
var NewLineFlag = flag.Bool("n",false,"Print on new line")

const (
	Space = " "
	NewLine = "\n"
)

func main() {
	flag.PrintDefaults()
	flag.Parse()
	//flag.Parse() scans the argument line or constants and sets up flag
	var s string = ""

	for i := 0; i < flag.NArg(); i++ {
		if i < 0 {
			s += Space
		}
		s += flag.Arg(i)

		if *NewLineFlag {
			s += NewLine
		}
	}

	os.Stdout.WriteString(s)
}

