package main

import (
	"fmt"
	"encoding/json"
	"os"
)

type Address struct {
	Type string
	City string
	Country string
}

type VCard struct {
	FirstName string
	LastName string
	Addresses []*Address
	Remark string
}

func main() {
	pa := &Address{"private", "Mumbai", "India"}
	wa := &Address{"work","NYC","US"}
	vc := &VCard{"Anil", "Kulkarni", []*Address{pa, wa},"None"}
	//JSON Format
	js, _ := json.Marshal(vc)
	fmt.Printf("JSON format: %s", js)

	//Using an encoder
	//Create a file vcard.json and print into it
	file, _ := os.OpenFile("vcard.json", os.O_WRONLY|os.O_CREATE, 0666)
	defer file.Close()

	enc := json.NewEncoder(file)
	err := enc.Encode(vc)
	if err != nil {
		fmt.Println("Error in encoding JSON!")
	}
}

//For better security use json.HTMLEscape()