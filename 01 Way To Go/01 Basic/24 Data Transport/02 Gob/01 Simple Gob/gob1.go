package main

import (
	"fmt"
	"encoding/gob"
	"bytes"
	"log"
)

type P struct {
	X, Y, Z int
	Name string
}

type Q struct {
	X, Y *int32
	Name string
}


func main() {
	//Create a gob encoder & decoder
	var network bytes.Buffer
	enc := gob.NewEncoder(&network)
	dec := gob.NewDecoder(&network)
	//encode (send) the value
	err := enc.Encode(P{3,4,5,"Pythagoras"})
	if err != nil {
		log.Fatal("encode error", err)
		fmt.Println("Fatal error occured during encoding!")
	}
	//Decoding (receiving) the message
	var q Q
	err = dec.Decode(&q)
	if err != nil {
		log.Fatal("decode error", err)
		fmt.Println("Fatal error occured during decoding!")
	}

	fmt.Printf("%q : {%d %d}", q.Name, *q.X, *q.Y)
}

