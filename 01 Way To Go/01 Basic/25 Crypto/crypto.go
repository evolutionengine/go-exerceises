package main

import (
	"fmt"
	"crypto/sha1"
	"log"
	"io"
)

func main() {
	hasher := sha1.New()
	io.WriteString(hasher, "Cryto Test")
	b := []byte{}
	fmt.Printf("Result: %x\n", hasher.Sum(b))
	fmt.Printf("Result: %v\n", hasher.Sum(b))
	fmt.Printf("Result: %d\n", hasher.Sum(b))

	hasher.Reset()

	data := []byte("Its gonna be over")

	n, err := hasher.Write(data)
	if n != len(data) || err != nil {
		log.Printf("Hash write error %v / %v\n", n, err)
	}

	checksum := hasher.Sum(b)
	fmt.Printf("Checksum Result: %x\n", checksum)
	checksum = hasher.Sum(data)
	fmt.Printf("Checksum Result: %x\n", checksum)
}
