package main

import (
	"fmt"
	"time"
)

func longwait()  {
	fmt.Println("Begining longwait()")
	time.Sleep(5 * 1e9) //sleep works in nanoseconds
	fmt.Println("End of longwait()")
}

func shortwait()  {
	fmt.Println("Begining shortwait()")
	time.Sleep(2 * 1e9)
	fmt.Println("End of shortwait()")
}

func main() {
	fmt.Println("In main()")
	go longwait()
	go shortwait()
	fmt.Println("ABout to sleep in main()")
	time.Sleep(10 * 1e9)
	fmt.Println("End of main()")
}

