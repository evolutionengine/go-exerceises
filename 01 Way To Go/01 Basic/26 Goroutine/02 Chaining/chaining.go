package main

import (
	"fmt"
	"flag"
)

var ngoroutine = flag.Int("n", 10000, "Number of Goroutines")

func f(left, right chan int)  {
	left <- 1+ <- right
}

func main() {
	flag.Parse()
	leftMost := make(chan int)
	var left, right chan int = nil, leftMost
	for i:= 0; i < *ngoroutine; i++ {
		left, right = right, make(chan int)
		go f(left, right)
	}
	right <- 0
	x := <-leftMost
	fmt.Println(x)
}