package main

import (
	"fmt"
	"time"
)
//pump - acts as a sender, it is also called as generator
func pump(ch chan int)  {
	for i := 0; ; i++{
		ch <- i
	}
}
//suck - acts as a receiver and hence completing communication
func suck(ch chan int)  {
	for {
		fmt.Println(<-ch)
	}
}

func main() {
	ch1 := make(chan int)
	go pump(ch1)
	go suck(ch1)
	time.Sleep(1e9)
}
