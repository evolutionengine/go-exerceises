package main

import (
	"fmt"
	"time"
)

//Instead of passing channel, let the function create channel and return it, this is called as factory pattern
func pump() chan int{
	ch := make(chan int)
	go func() {
		for i := 0; ; i++ {
			ch <- i
		}
	}()
	return ch
}

func suck(ch chan int) {
	for {
		fmt.Println(<-ch)
	}
}

func main() {
	stream := pump()
	go suck(stream)
	//go suck( pump())
	time.Sleep(1e9)
}