package _1_Sieve

import (
	"fmt"
)

func generate(ch chan int)  {
	for i := 2; ; i++ {
		ch <- i //Send i to ch channel
	}
}

func filter(in, out chan int, prime int)  {
	for {
		i := <-in //receive value of 'in' in 'i'
		if i%prime != 0 {
			out <- i //send i to channel out
		}
	}
}

func main() {
	ch := make(chan int)
	go generate(ch)
	for {
		prime := <- ch
		fmt.Print(prime, " ")
		ch1 := make(chan int)
		go filter(ch,ch1, prime)
		ch = ch1
	}
}