package main

import (
	"fmt"
	"runtime"
	"time"
)

func pump1() chan int {
	ch := make(chan int)
	go func() {
		for i := 0; ; i++ {
			ch <- i * 2
		}
	}()
	return ch
}

func pump2() chan int {
	ch := make(chan int)
	go func() {
		for i := 0; ; i++ {
			ch <- i + 5
		}
	}()
	return ch
}

func suck(ch1, ch2 chan int) {
	for {
		select {
		case v := <-ch1:
			fmt.Printf("Received %d on channel 1\n", v)
		case v := <-ch2:
			fmt.Printf("Received %d on channel 2\n", v)
		}
	}
}

func main() {
	runtime.GOMAXPROCS(2)
	ch1 := pump1()
	ch2 := pump2()

	go suck(ch1, ch2)
	time.Sleep(1e9)
}
