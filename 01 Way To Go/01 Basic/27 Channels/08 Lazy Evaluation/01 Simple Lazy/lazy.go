//At a time only evaluates what is required, thus saving CPU & Memory
package _1_Simple_Lazy

import "fmt"

var resume chan int

func integers() chan int {
	yield := make(chan int)
	count := 0
	go func() {
		for {
			yield <- count
			count++
		}
	}()
	return yield
}

func generateIntegers() int {
	return <- resume
}

func main() {
	resume = integers()
	fmt.Println(generateIntegers())
	fmt.Println(generateIntegers())
	fmt.Println(generateIntegers())
}