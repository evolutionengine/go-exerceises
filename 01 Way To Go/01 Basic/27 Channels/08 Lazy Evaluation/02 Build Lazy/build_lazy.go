//Not Working !!!!
//Todo - Refactor Program !

package main

import "fmt"

type Any interface{}

type EvalFunc func(Any) (Any, Any)

func BuildLazyEvaluator(evalFunc EvalFunc, initState Any) func() Any {
	retValueChan := make(chan int)
	loopFunc := func() {
		actState := initState
		var retValue Any
		for {
			retValue, actState = evalFunc(actState)
			retValueChan <- retValue
		}
	}
	retFunc := func() Any {
		return <-retValueChan
	}

	go loopFunc()
	return retFunc
}

func BuildLazyIntEvaluator(evalFunc EvalFunc, initState Any) func() int {
	ef := BuildLazyEvaluator(evalFunc, initState)
	return func() {
		return ef().(int)
	}
}

func main() {
	evenFunc := func(state Any) (Any, Any) {
		os := state.(int)
		ns := os + 2
		return os, ns
	}

	even := BuildLazyIntEvaluator(evenFunc, 0)

	for i := 0; i < 10; i++ {
		fmt.Printf("%vth even: %v\n", i, even())
	}
}
