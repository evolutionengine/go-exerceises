* If two goroutines have to communicate they should be given the same channel parameter

* For a channel to operate it requires a sender & receiver, otherwise the communication is blocked.