_This Creates Italic_

**This Creates Bold**

_**This Has Both Italic & Bold**_

# Header 1
## Header 2
### Header 3
#### Header 4
##### Header 5
###### Header 6

[Visit Github!](www.github.com)

![Benjamin Bannekat](https://octodex.github.com/images/bannekat.png)

> "In a few moments he was barefoot, his stockings folded in his pockets and his
  canvas shoes dangling by their knotted laces over his shoulders and, picking a
  pointed salt-eaten stick out of the jetsam among the rocks, he clambered down
  the slope of the breakwater."
  
  * Milk
  * Eggs
  * Salmon
  * Butter
  
  1. Crack three eggs over a bowl
  2. Pour a gallon of milk into the bowl
  3. Rub the salmon vigorously with butter
  4. Drop the salmon into the egg-milk bowl
  
  * Tintin
     * A reporter
    * Has poofy orange hair
    * Friends with the world's most awesome dog
  * Haddock
    * A sea captain
    * Has a fantastic beard
    * Loves whiskey
     * Possibly also scotch?
     
Do I contradict myself?..
Very well then I contradict myself,..
(I am large, I contain multitudes.)