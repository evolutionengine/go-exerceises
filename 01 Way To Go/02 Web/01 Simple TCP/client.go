package main

import (
	"fmt"
	"net"
	"bufio"
	"os"
	"strings"
)

func main() {
	//Open connection
	conn, err := net.Dial("tcp", "localhost:50000")
	if err != nil {
		//No connection, because the target machine actively refused it
		fmt.Println("Error dialing...", err.Error())
		return
	}

	inputReader := bufio.NewReader(os.Stdin)
	fmt.Println("What is your name ?")
	clientName, _ := inputReader.ReadString('\n')
	fmt.Printf("Client name: %s\n", clientName)

	trimmedClient := strings.Trim(clientName, "\n")
	fmt.Printf("Trimmed client: %s\n", trimmedClient)

	for {
		fmt.Printf("What do you want to send to server ? Or Type Q to quit")
		input, _ := inputReader.ReadString('\n')
		fmt.Printf("Input: %s\n", input)
		trimmedInput := strings.Trim(input, "\n")
		fmt.Printf("Trimmed Input: %s\n", trimmedInput)

		if trimmedInput == "Q" {
			return
		}

		_, err = conn.Write([]byte(trimmedClient + " says: " + trimmedInput))
	}

}

