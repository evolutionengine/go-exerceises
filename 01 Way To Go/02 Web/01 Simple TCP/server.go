package main

import (
	"fmt"
	"net"
)

func doServerStuff(conn net.Conn)  {
	for {
		buf := make([]byte, 512)
		_, err := conn.Read(buf)
		if err != nil {
			fmt.Println("Error reading...", err.Error())
			return
		}
		fmt.Printf("Received data %v from connection: \n", string(buf))
	}
}

func main() {
	fmt.Println("Starting server...")
	//create listener
	listener, err := net.Listen("tcp", "localhost:50000")
	if err != nil {
		fmt.Println("Error listening", err.Error())
		return //terminate the program
	}
	//listen and accept connections from client
	for {
		conn, err := listener.Accept()
		if err != nil {
			fmt.Println("Error accepting connection...", err.Error())
			return
		}
		go doServerStuff(conn)
	}
}

