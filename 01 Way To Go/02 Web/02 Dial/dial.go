package main

import (
	"fmt"
	"net"
	"os"
)

func checkCOnnection(conn net.Conn, err error)  {
	if err != nil {
		fmt.Printf("Error connecting: %v\n", err.Error())
		os.Exit(1)
	}
	fmt.Println("Connection is made with: ", conn)
}

func main() {
	conn, err := net.Dial("tcp", "192.0.32.10:80") //tcp ipv4
	checkCOnnection(conn, err)

	conn, err = net.Dial("udp", "92.0.32.10:80") //udp
	checkCOnnection(conn, err)

	conn, err = net.Dial("tcp", "[2620:0:2d0:200::10]:80") //tcp ipv6
	checkCOnnection(conn, err)
}

