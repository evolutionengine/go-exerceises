package main

import (
	"fmt"
	"net"
	"io"
)

func main() {
	var (
		host = "www.apache.org"
		port = "80"
		remote = host + ":" + port
		msg = "GET / \n"
		data = make([]uint8, 4096)
		read = true
		count = 0
	)

	//create the socket
	conn, err := net.Dial("tcp", remote)
	io.WriteString(conn, msg)
	//read response from web server
	for read {
		count, err = conn.Read(data)
		read = (err == nil)
		fmt.Printf(string(data[0:count]))
	}
	conn.Close()
}

