//simple multi thread / multi core tcp
package main

import (
	"fmt"
	"net"
	"syscall"
	"flag"
)

const maxRead = 25

func initServer(hostAndPort string) *net.TCPListener {
	serverAddr, err := net.ResolveTCPAddr("tcp", "hostAndPort")
	checkError(err, "Resolving address:port failed " + hostAndPort +"'")
	listerner, err := net.ListenTCP("tcp", serverAddr)
	fmt.Println("Listening to: ", listerner.Addr().String())
	return listerner
}

func connectionHandler(conn net.Conn) {
	connFrom := conn.RemoteAddr().String()
	fmt.Println("Connection from: ", connFrom)
	sayHello(conn)
	for {
		var ibuf []byte = make([]byte, maxRead + 1)
		length, err :=conn.Read(ibuf[0:maxRead])
		ibuf[maxRead] = 0 //to avoid overflow
		switch err {
		case nil:
			handleMsg(length, err, ibuf)
		case syscall.EAGAIN:
			continue
		default:
			goto DISCONNECT
			
		}
	}

	DISCONNECT:
		err := conn.Close()
		fmt.Println("Connection closed: ", connFrom)
		checkError(err, "Close: ")
}

func sayHello(to net.Conn)  {
	obuf := []byte{'H', 'E', 'L', 'L', 'O', ' ', 'G', 'O', '!'}
	wrote, err := to.Write(obuf)
	checkError(err, "Write: wrote " + string(wrote) + " bytes." )
}

func handleMsg(length int, err error, msg []byte)  {
	if length > 0 {
		fmt.Println("<", length, ":")
		for i := 0; ;i++ {
			if msg[i] == 0 {
				break
			}
			fmt.Printf("%c", msg[i])
		}
		fmt.Println(">")
	}
}

func checkError(err error, info string)  {
	if err != nil {
		panic("Error: " + info + err.Error()) //terminate program
	}
}

func main() {
	flag.Parse()
	if flag.NArg() != 2 {
		panic("Usage: host port")
	}
	hostAndPort := fmt.Sprintf("%s:%s", flag.Arg(0), flag.Arg(1))
	listener := initServer(hostAndPort)
	for {
		conn, err := listener.Accept()
		checkError(err, "Accept: ")
		go connectionHandler(conn)
	}
}

