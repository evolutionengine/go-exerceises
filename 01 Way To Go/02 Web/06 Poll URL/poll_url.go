package main

import (
	"net/http"
	"fmt"
)

var urls = []string{
	"http://www.google.com",
	"http://www.golang.org",
	"http://blog.golang.org",
	"http://www.indietalkies.com",
	"http://chefmaker.net",
}

func main() {
	//Execute HEAD request for all the urls
	//and return http return string or error string
	for _, url := range urls {
		resp, err := http.Head(url)
		if err != nil {
			fmt.Println("Error: ", url, err.Error())
			return
		}
		fmt.Println("Response from", url, ":", resp.Status)

	}
}
