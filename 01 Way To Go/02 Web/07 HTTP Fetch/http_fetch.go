package main

import (
	"log"
	"net/http"
	"io/ioutil"
	"fmt"
)

func checkError(err error)  {
	if err != nil {
		log.Fatalf("Get: %v", err)
	}
}

func main() {
	res, err := http.Get("http://www.google.com")
	checkError(err)
	data, err := ioutil.ReadAll(res.Body)
	checkError(err)
	fmt.Printf("Got data: %s\n", string(data))
}
