package main

import (
	"encoding/xml"
	"net/http"
	"fmt"
	"io/ioutil"
)

//These structs will house the unmarshelled response
//They should be hierarichaly shaped like a XML
type Status struct {
	Text string
}

type User struct {
	XMLName xml.Name
	Status Status
}

func main() {
	response, err := http.Get("http://twitter.com/user/indietalkies.xml")
	if err != nil {
		fmt.Printf("Error: %v", err.Error())
	}
	user := User{xml.Name{"", "user"}, Status{""}}
	//unmarshal xml in struct
	data, err := ioutil.ReadAll(response.Body)

	if err != nil {
		fmt.Printf("Error: %s", err.Error())
		return
	}
	xml.Unmarshal(data, &user)

	fmt.Printf("Status: %s", user.Status.Text)
}
