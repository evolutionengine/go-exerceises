package main

import (
	"net/http"
	"io"
)

const form = `<html><body><form action="#" method="post" name="bar">
				<input type="text" name="in" />
				<input type="submit" value="submit" />
			  </form></body></html>`

//handle a simple get request
func SimpleServer(w http.ResponseWriter, request *http.Request)  {
	io.WriteString(w, "<h1>Hello, World!</h1>")
}

//handle get & post requests from a 'form
func FormServer(w http.ResponseWriter, request *http.Request) {
	w.Header().Set("Content-type", "text/html")
	switch request.Method {
	case "GET":
		//	display form to the user
		io.WriteString(w, form)
	case "POST":
		//handle the form data
		request.ParseForm()
		io.WriteString(w, request.FormValue("in"))
	}
}

func main() {
	http.HandleFunc("/test1", SimpleServer)
	http.HandleFunc("/test2", FormServer)
	if err := http.ListenAndServe(":8088", nil); err != nil {
		panic(err)
	}
}
