#HTTP Status Codes
###Are defined as Go constants

* http.StatusContinue = 100
* http.StatusOk = 200
* http.StatusFound = 302
* http.StatusBadRequest = 400
* http.StatusUnathourized = 401
* http.StatusForbidden = 403
* http.StatusNotFound = 404
* http.StatusInternalServerError = 500