package main

import (
	"net/http"
	"./data"
	"errors"
)

func session(w http.ResponseWriter, r *http.Response) (sess data.Session, err error) {
	cookie, err := r.Cookie("_cookie")
	if err == nil {
		sess = data.Session{UserId:cookie.Value}
		if ok, _ := sess.Check(); !ok {
			err = errors.New("Invalid Session")
		}
	}
	return
}
