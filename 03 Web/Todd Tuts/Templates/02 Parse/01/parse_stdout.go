package main

import (
	"log"
	"os"
	"text/template"
)

func main() {
	dir, err := template.ParseFiles("t0.tmpl")
	if err != nil {
		log.Fatal(err)
	}

	err = dir.Execute(os.Stdout, nil)
	if err != nil {
		log.Fatal(err)
	}
}

// Do not use this code for production!
