package main

import (
	"log"
	"os"
	"text/template"
)

var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseGlob("t0.gohtml"))
}

func main() {

	sages := map[string]string{
		"India":       "Gadhi",
		"America":     "Red India",
		"Middle East": "Rumi",
		"Bodhgaya":    "BUddha",
	}
	err := tpl.ExecuteTemplate(os.Stdout, "t0.gohtml", sages)
	if err != nil {
		log.Fatalln(err)
	}
}
