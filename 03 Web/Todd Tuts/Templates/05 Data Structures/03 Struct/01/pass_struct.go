package main

import (
	"log"
	"os"
	"text/template"
)

var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseGlob("t0.gohtml"))
}

type sage struct {
	Name string
	Moto string
}

func main() {
	buddha := sage{
		Name: "Buddha",
		Moto: "Silence is peace",
	}
	err := tpl.ExecuteTemplate(os.Stdout, "t0.gohtml", buddha)
	if err != nil {
		log.Fatalln(err)
	}
}
