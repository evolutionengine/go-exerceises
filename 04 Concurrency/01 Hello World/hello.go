package main

import (
	"sync"
	"fmt"
)

func main() {
	var wg sync.WaitGroup
	sayHello := func() {
		defer wg.Done()
		fmt.Println("Hello, World!")
	}
	wg.Add(1)
	go sayHello()
	wg.Wait()
}
