//Bubble Sort - Following is an implementation of Bubble Sort algorithm
package main

import (
	"fmt"
	"math/rand"
	"time"
)
//Generate ints and enter into slice
func generateSlice(size int) []int {
	newList := make([]int, size, size)
	rand.Seed(time.Now().UnixNano())
	//populating the slice
	for i := 0; i < size; i++ {
		newList[i] = rand.Intn(999) - rand.Intn(999)
	}
	return newList
}
//BubbleSort - sorting the elements in slice using bubble sort
func BubbleSort(items []int) {
	n := len(items)
	sorted := false
	//loop till sorted = true
	for !sorted {
		//setting initial swap condition
		swapped := false
		for i := 0; i < n-1; i++ {
			//swap elements if required
			if items[i+1] < items[i]  {
				items[i+1], items[i] = items[i], items[i+1]
				swapped = true
			}
		}
		//setting condition to exit from loop
		if !swapped {
			sorted = true
		}
		n = n -1
	}
}

func main() {
	randomList := generateSlice(35)
	fmt.Println("Unsorted Slice: ", randomList)
	BubbleSort(randomList)
	fmt.Println("Sorted List: ", randomList)
}