package main

import (
	"fmt"
	"sync"
	"time"
)

type Job struct {
	i    int
	max  int
	text string
}

func outputText(j *Job, w *sync.WaitGroup) {
	for j.i < j.max {
		time.Sleep(1 * time.Millisecond)
		fmt.Println(j.text)
		j.i++
	}
	w.Done()
}

func main() {
	wg := new(sync.WaitGroup)
	fmt.Println("Printing:")

	hello := new(Job)
	world := new(Job)

	hello.i = 0
	hello.max = 3
	hello.text = "hello"

	world.i = 0
	world.max = 5
	world.text = "world"

	go outputText(hello, wg)
	go outputText(world, wg)
}
