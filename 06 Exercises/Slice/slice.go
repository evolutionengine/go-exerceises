package main

import (
	"fmt"
	"reflect"
)

func main() {
	// Declaring a slice, variable := make(type, length, capacity)
	// When capacity is not mentioned, then length = capacity
	/*
	  s1 := make([]int, 5)
	  len(s1) = 5
	  cap(s1) = 5
	*/
	s1 := make([]int, 10, 50)
	fmt.Printf("The length of slice is: %v \tAnd the capacity is: %v\n", len(s1), cap(s1))
	// Generating data
	for i := 0; i < len(s1); i++ {
		s1[i] = i
	}
	// Printing the slice
	for key, value := range s1 {
		fmt.Printf("Position: %v\tValue: %v\n", key, value)
	}

	/*
	  Increasing the length of Slice
	  To increase upto capacity -> s1 = s1[:cap(s1)]
	*/
	fmt.Println("\nIncreasing the length of slice by 10")
	for i := 0; i < 20; i++ {
		s1 = s1[0 : i+1] //Reslicing here
	}
	fmt.Printf("The length of new slice is: %v \tAnd the capacity is: %v\n", len(s1), cap(s1))
	// Creating Values
	for i := 10; i < len(s1); i++ {
		s1[i] = i
	}
	// Printing new values
	for index, value := range s1 {
		fmt.Printf("Position: %v\tValue: %v\n", index, value)
	}
	/*
		Slice Index Operations
	*/
	fmt.Println("\nFirst three elements in slice are: ", s1[:3])
	fmt.Println("Last three elements in slice are: ", s1[len(s1)-3:])
	fmt.Println("Three elements from index: 10 in slice are: ", s1[9:12])

	/*
		Copying Elements
	*/
	sliceGolang := []byte{'g', 'o', 'l', 'a', 'n', 'g'}
	fmt.Println("\nSlice: ", string(sliceGolang))
	// Copying slective elements into new slice
	sliceGo := sliceGolang[:2]
	fmt.Printf("Creating a new slice: '%s'\n", string(sliceGo))
	// editing elements
	sliceGo[0] = 'b'
	fmt.Println(string(sliceGo))

	/*
		Deleting Elements
	*/
	names := []string{"john", "racheal", "snow", "edd", "stark"}
	fmt.Println("\nOriginal Slice: ", names)
	// deleting 2nd element
	names = append(names[:1], names[1+1:]...)
	fmt.Println("Slice after deleting element at 1:", names)

	/*
		Copying Slices
	*/
	moreNames := []string{"khalisi", "drago", "dragon"}
	newNames := append(names, moreNames...) //Appending to an existing slice
	fmt.Println("\nNew slice after copying:", newNames)
	// Copying to a new slice
	fewMoreNames := make([]string, len(names))
	fewMoreNames = append(fewMoreNames, newNames...)
	fmt.Println("\nSlice after copying in a blank slice:", fewMoreNames)

	/*
		Inserting into slice
	*/
	weekdays := []string{"sunday", "monday", "wedensday", "thrusday", "friday", "saturday"}
	weekdays = append(weekdays[:2], append([]string{"tuesday"}, weekdays[2:]...)...)
	fmt.Println("\nWeekdays are:", weekdays)

	// Refer this link for more details - https://github.com/golang/go/wiki/SliceTricks

	/*
		Comparing slices
	*/
	months := []string{"jan", "feb", "mar", "apr", "may"}
	calenderMonths := []string{"jan", "feb", "mar", "apr", "may"}
	fmt.Println("\nAre both the slices equal:", reflect.DeepEqual(months, calenderMonths))
}
